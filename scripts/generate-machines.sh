#!/bin/bash

POKY_DIR="../poky"

SYSTEMS="qemu-system-aarch64 qemu-system-arm"
QEMU_PATH="/usr/bin"
#QEMU_PATH="/builder/meta-qemu-machines/build/tmp/work/x86_64-linux/qemu-helper-native/1.0-r1/recipe-sysroot-native/usr/bin/"

rm recipes-kernel/linux/linux-yocto_%.bbappend
rm ci/machine/*
rm conf/machine/*
sed -i 59,100d .gitlab-ci.yml

for SYSTEM in $SYSTEMS; do
	CPUS=$($QEMU_PATH/$SYSTEM -machine virt -cpu ? | sed /Available/d)
	if [[ $SYSTEM == "qemu-system-aarch64" ]]; then
		VIRTIO="pci"
	else
		VIRTIO="device"
	fi

	for CPU in $CPUS; do
		echo $CPU

		# HACK, do smp too large to prevent qemu from starting, but since the error isn't "not supported" then it will not return 0
		$QEMU_PATH/$SYSTEM -M virt -cpu $CPU -smp 99 2>&1 | grep "not supported"
		if [ $? -eq 0 ]; then
			echo "invalid machine"
			continue
		fi

		# Create machine conf file

		TUNE_CPU=$(echo $CPU | sed 's/-//g')
		FILE=$(find $POKY_DIR -name tune-$TUNE_CPU.inc)
		if [[ $FILE == "" ]]; then
			echo "tune-$TUNE_CPU.inc not found"
			continue
		fi
		FILE=$(echo $FILE | sed "s#$POKY_DIR/meta/##g" )

		#Create the file
		NAME="qemu-$CPU"
		CONF_NAME="$NAME.conf"
		echo "require conf/machine/include/qemu.inc" > conf/machine/$CONF_NAME
		echo "require $FILE" >> conf/machine/$CONF_NAME

		echo -e '\nSERIAL_CONSOLES ?= "115200;ttyAMA0 115200;hvc0"\nSERIAL_CONSOLES_CHECK = "${SERIAL_CONSOLES}"' >> conf/machine/$CONF_NAME

		echo -e '\n# u-boot config"' >> conf/machine/$CONF_NAME
		if [[ $SYSTEM == "qemu-system-aarch64" ]]; then
			echo 'UBOOT_MACHINE ?= "qemu_arm64_defconfig"' >> conf/machine/$CONF_NAME
		elif [[ $SYSTEM == "qemu-system-arm" ]]; then
			echo 'UBOOT_MACHINE ?= "qemu_arm_defconfig"' >> conf/machine/$CONF_NAME
		else
			echo "invalid qemu arch"
			exit
		fi

		echo -e '\n# Linux kernel config' >> conf/machine/$CONF_NAME
		if [[ $SYSTEM == "qemu-system-aarch64" ]]; then
			echo 'KERNEL_IMAGETYPE = "Image"' >> conf/machine/$CONF_NAME
			echo 'KBUILD_DEFCONFIG = "defconfig"' >> conf/machine/$CONF_NAME
			echo 'KCONFIG_MODE = "--alldefconfig"' >> conf/machine/$CONF_NAME
		elif [[ $SYSTEM == "qemu-system-arm" ]]; then
			echo 'KERNEL_IMAGETYPE = "zImage"' >> conf/machine/$CONF_NAME
			echo 'KBUILD_DEFCONFIG  = "multi_v7_defconfig"' >> conf/machine/$CONF_NAME
			echo 'KCONFIG_MODE = "--alldefconfig"' >> conf/machine/$CONF_NAME
		fi

		echo -e '\n# runqemu config' >> conf/machine/$CONF_NAME
		echo 'QB_SYSTEM_NAME = "'$SYSTEM'"' >> conf/machine/$CONF_NAME
		echo 'QB_MACHINE = "-machine virt"' >> conf/machine/$CONF_NAME
		echo 'QB_CPU = "-cpu '$CPU'"' >> conf/machine/$CONF_NAME
		#echo 'QB_SMP ?= "-smp 4"' >> conf/machine/$CONF_NAME
		echo 'QB_GRAPHICS = "-device virtio-gpu-'$VIRTIO'"' >> conf/machine/$CONF_NAME
		echo 'QB_OPT_APPEND = "-device qemu-xhci -device usb-tablet -device usb-kbd"' >> conf/machine/$CONF_NAME
		echo 'QB_TAP_OPT = "-netdev tap,id=net0,ifname=@TAP@,script=no,downscript=no"' >> conf/machine/$CONF_NAME
		echo 'QB_NETWORK_DEVICE = "-device virtio-net-'$VIRTIO',netdev=net0,mac=@MAC@"' >> conf/machine/$CONF_NAME
		echo 'QB_ROOTFS_OPT = "-drive id=disk0,file=@ROOTFS@,if=none,format=raw -device virtio-blk-'$VIRTIO',drive=disk0"' >> conf/machine/$CONF_NAME
		echo 'QB_SERIAL_OPT = "-device virtio-serial-'$VIRTIO' -chardev null,id=virtcon -device virtconsole,chardev=virtcon"' >> conf/machine/$CONF_NAME
		echo 'QB_TCPSERIAL_OPT = "-device virtio-serial-'$VIRTIO' -chardev socket,id=virtcon,port=@PORT@,host=127.0.0.1 -device virtconsole,chardev=virtcon"' >> conf/machine/$CONF_NAME


		# Add to COMPAT

		echo 'COMPATIBLE_MACHINE:'$NAME' = "'$NAME'"' >> recipes-kernel/linux/linux-yocto_%.bbappend


		# Create kas yml file

		echo -e 'header:\n  version: 14\n  includes:\n    - ci/base.yml\n' > ci/machine/$NAME.yml
		echo 'machine: '$NAME  >> ci/machine/$NAME.yml

		# add to gitlab ci file
		echo -e "\n$NAME:\n  extends: .build\n  parallel:\n    matrix:\n      - TESTING: testimage" >> .gitlab-ci.yml
	done
done
